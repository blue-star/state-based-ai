﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateAI
{
    public class StateAI
    {
        internal State CurrentState = null;
        internal Dictionary<State, StateAction> PreviousStates = new Dictionary<State, StateAction>();

        internal List<State> AllStates = new List<State>();

        private bool didPerformAction = false;

        public State SetState(string stateString)
        {
            didPerformAction = false;
            State existingState = AllStates.Find(o => o.StateString == stateString);

            if (existingState != null)
            {
                CurrentState = existingState;
                return existingState;
            }

            State newState = new State();
            CurrentState = newState;
            AllStates.Add(newState);
            return newState;
        }

        public void PerformAction()
        {
            if (didPerformAction)
                throw new InvalidOperationException("An action has already been performed during this state!");

            didPerformAction = true;

            StateAction chosenAction = null;

            foreach (StateAction action in CurrentState.PossibleActions)
            {
                if (chosenAction == null)
                {
                    chosenAction = action;
                }
                else
                {
                    if (chosenAction.Score < action.Score)
                        chosenAction = action;
                }
            }

            chosenAction.ExecuteAction();

            PreviousStates.Add(CurrentState, chosenAction);
        }

        public void RecordVictory()
        {
            foreach(KeyValuePair<State, StateAction> record in PreviousStates)
            {
                record.Value.CanResultInVictory = true;
            }
        }

        public void RecordLoss()
        {
            foreach (KeyValuePair<State, StateAction> record in PreviousStates)
            {
                record.Value.CanResultInLoss = true;
            }
        }
    }
}
