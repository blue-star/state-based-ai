﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateAI
{
    public class StateAction
    {
        public bool CanResultInVictory { get; internal set; }
        public bool CanResultInLoss { get; internal set; }
        public int Index { get; private set; }

        public OnActionExecutionDelegate OnActionExecution;

        public delegate void OnActionExecutionDelegate(StateAction action, int index);

        public int Score
        {
            get
            {
                return 0 + (CanResultInVictory ? 2 : 0) - (CanResultInLoss ? -1 : 0);
            }
        }

        public StateAction(int index)
        {
            Index = index;
        }
        internal void ExecuteAction()
        {
            OnActionExecution?.Invoke(this, Index);
        }

        internal void RecordLoss()
        {
            CanResultInLoss = true;
        }

        internal void RecordVictory()
        {
            CanResultInVictory = true;
        }
    }
}
