﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StateAI
{
    public class State
    {
        public bool StateIsInitialized { get; internal set; }
        public StateAction[] PossibleActions { get; internal set; }
        public string StateString { get; internal set; }

        public void InitializeState(int actions, string stateString)
        {
            StateIsInitialized = true;

            PossibleActions = new StateAction[actions];

            StateString = stateString;

            for(int i = 0; i < PossibleActions.Length; i++)
            {
                PossibleActions[i] = new StateAction(i);
            }
        }
    }
}
